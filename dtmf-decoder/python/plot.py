from matplotlib import pyplot as plt

text_file = open("pic.dat", "r")
lines = text_file.readlines()
lines = [int(i) for i in lines]
marker_style = dict(linestyle=':', color='cornflowerblue', markersize=10)
plt.plot(lines, **marker_style)
plt.ylabel('some numbers')
plt.show()
