'''
===================================
3D wireframe plots in one direction
===================================

Demonstrates that setting rstride or cstride to 0 causes wires to not be
generated in the corresponding direction.
'''

with open('double.csv') as f:
    lines = list(line.split(',') for line in f.read().splitlines())
    doubles = []
    for line in lines:
        arr = []
        for s in line:
            arr.append(float(s))
        doubles.append(arr)

import matplotlib.pyplot as plt
import numpy as np

dd = doubles
block_size = len(dd[0])
freq_step = 4000 / block_size
block_size * 2
time_step = block_size * 2 * (1 / 8000.0)

X = [i * freq_step for i in range(0, len(dd[0]))]
Y = [i * time_step for i in range(1, len(dd) + 1)]
print
X, Y = np.meshgrid(X, Y)
print

fig = plt.figure(figsize=(12, 9))
ax2 = fig.gca(projection='3d')

Z = np.array(dd)

# Get the test data

# Give the first plot only wireframes of the type y = c
# ax1.plot_wireframe(X, Y, Z, rstride=10, cstride=0)
# ax1.set_title("Column (x) stride set to 0")

# Give the second plot only wireframes of the type x = c
stride = len(dd) / 200
if stride == 0:
    stride = 1

ax2.plot_wireframe(X, Y, Z, rstride=stride, cstride=0)
# ax2.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0, antialiased=True)
ax2.set_title("Row (y) stride set to 0")

ax2.set_xlabel('FREQ -> ')
ax2.set_ylabel('Time -> ')
ax2.set_zlabel('Amplitude')

plt.tight_layout()
plt.show()