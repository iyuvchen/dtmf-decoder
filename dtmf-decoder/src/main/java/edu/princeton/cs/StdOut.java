package edu.princeton.cs;

/**
 * @author iyuvchenko 2018
 */
public class StdOut {

    public static void println(String str){
        System.out.println(str);
    }

    public static void println() {
        System.out.println();
    }

    public static void println(Complex complex) {
        System.out.println(complex.toString());
    }
}
