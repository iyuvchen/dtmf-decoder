package org.amd.dtmfDecoder;

import ch.qos.logback.classic.Level;
import org.amd.dtmfDecoder.analyser.FreqRecognizer;
import org.amd.dtmfDecoder.analyser.IEventDispatcher;
import org.amd.dtmfDecoder.analyser.IRecognizer;
import org.amd.dtmfDecoder.analyser.SpectrumAnalyser;
import org.amd.dtmfDecoder.analyser.dtmf.DtmfEvent;
import org.amd.dtmfDecoder.micCapture.MicReader;
import org.amd.dtmfDecoder.utils.AudioFileWriter;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.function.Consumer;

/**
 * @author iyuvchenko 2018
 */
public class RealTimeAnalyser {
    private static final Logger logger = LoggerFactory.getLogger(RealTimeAnalyser.class);
    private static short[] allData = {};

    public static void main(String[] args) throws InterruptedException {
        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("org.dtmfDecoder")).setLevel(Level.WARN);

        System.out.println(DtmfUtils.readLogo());
        System.out.println("Punch few numbers on your phone in the next 50s");
        System.out.println("Recognized digits: ");


        final Consumer<DtmfEvent> callback = dtmfEvent -> {
            logger.debug("----------- got DTMF {}", dtmfEvent);
            System.out.print(dtmfEvent.getValue());
            System.out.flush();
        };

        final IRecognizer recognizer = new FreqRecognizer(callback);
        final IEventDispatcher eventDispatcher = IEventDispatcher.createDefault(recognizer, DtmfUtils.BLOCK_SIZE / 2);

        final SpectrumAnalyser analyser = new SpectrumAnalyser(eventDispatcher);

        final Consumer<short[]> micCallback = arr -> {
            logger.debug("got {} samples from MicReader", arr.length);
            analyser.submitForAnalysis(arr);
            allData = DtmfUtils.joinTwoArrays(allData, arr);
        };




        MicReader micReader = new MicReader(micCallback);
        Thread t1 = new Thread(micReader);
        t1.setDaemon(true);
        t1.start();
        Thread.sleep(50000);
        micReader.shutdown();
        Thread.sleep(2000);

        final String suff = args.length == 0 ? "" : args[0];
        final String fName = "realtime" + suff + ".au";
        System.out.println();
        logger.debug("got DTMF end Au File {}", fName);
        final File f = new File(fName);
        new AudioFileWriter(allData, f).write();
        System.out.println("Audio recording with your digits saved to " + fName);
        System.exit(0);
    }

}
