package org.amd.dtmfDecoder.analyser;

import com.google.common.collect.EvictingQueue;
import org.amd.dtmfDecoder.analyser.message.AccumulateEvent;
import org.amd.dtmfDecoder.analyser.message.SignalEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @author iyuvchenko 2018
 */
public class EnergyCaptor implements IEnergyCaptor {
    private static final Logger logger = LoggerFactory.getLogger(EnergyCaptor.class);

    private static final double SIGNAL_NOISE_RATIO_THRESHOLD = 50;

    //IEnergyCaptor will stay silent(not firing Trigger) for next SILENT_COUNT accumulate events
    private static final int SILENT_COUNT = 7;

    private static final double ENERGY_THRESHOLD = 30_000.0 * 30_000.0 + 30_000.0 * 30_000.0;

    private EvictingQueue<SignalEvent> last4Events = EvictingQueue.create(4);
    private EvictingQueue<SignalEvent> last10Events = EvictingQueue.create(10);
    private List<SignalEvent> allSignalEvents = new LinkedList<>();

    private final int myFrequency;
    private double last4CumulativeEnergy = 0;
    private double last4AverageEnergy = 0;
    private double last10CumulativeEnergy = 0;
    private double last10AverageEnergy = 0;
    private double cumulativeSignalNoiseRatio = 0;
    private double maxCumulativeSignalNoiseRatio = 0;
    private double maxLast4CumulativeEnergy = 0;

    private final BlockingDeque<AccumulateEvent> blockingDequeue = new LinkedBlockingDeque<>(1);
    private final ExecutorService executorService;

    public EnergyCaptor( final int myFrequency, final ExecutorService executorService) {
        this.myFrequency = myFrequency;
        this.executorService = executorService;
    }

//    /**
//     * Every time energy level exceeds some threshold consumer callback is fired
//     */
//    private void pullTrigger() {
//        log("\t Pulling Trigger with SIGNAL_NOISE_RATIO_THRESHOLD: {} and ENERGY_THRESHOLD: {}", SIGNAL_NOISE_RATIO_THRESHOLD, ENERGY_THRESHOLD);
//        callback.accept(new EnergyTriggerEvent(this.myFrequency, this.cumulativeSignalNoiseRatio, last4CumulativeEnergy));
//    }

    @Override
    public void accumulate(final AccumulateEvent accumulateEvent) {
        try {
            blockingDequeue.putLast(accumulateEvent);
            logger.debug("submit accumulate {}", accumulateEvent);
        } catch (InterruptedException e) {
            logger.error("IEnergyCaptor[{}] Submit accumulate resulted in ",myFrequency, e);
            throw new RuntimeException(e);
        }

        executorService.submit(this::checkEvents);
    }

    private synchronized void checkEvents() {

        final AccumulateEvent accumulateEvent = blockingDequeue.poll();
        if(accumulateEvent!=null) {
                calcPower(accumulateEvent.toSignalEvent());
        }
    }

    private void calcPower(SignalEvent signalEvent) {
        log("calcPower starting {} ", signalEvent);
        if (signalEvent.getFrequency() != myFrequency)
            throw new RuntimeException("It's not my frequency!!!");
        last4Events.add(signalEvent);
        last10Events.add(signalEvent);
        allSignalEvents.add(signalEvent);

        last4CumulativeEnergy = last4Events.stream().map(SignalEvent::getAmplitude)
            .map(dbl -> dbl * dbl).mapToDouble(i -> i).sum();

        long length = last10Events.size() - last4Events.size();
        if(signalEvent.getAmplitude()!=0.0){
            System.out.print("");
        }
        last10CumulativeEnergy = last10CumulativeEnergy(length);

        //check if captured enough spectrum frames.
        if (length == 6 && last10CumulativeEnergy != 0.0) {

            last4AverageEnergy = last4CumulativeEnergy / last4Events.size();
            last10AverageEnergy = last10CumulativeEnergy / length;

            cumulativeSignalNoiseRatio = last4AverageEnergy / last10AverageEnergy;
            log("\t cumulativeSignalNoiseRatio: {} last4CumulativeEnergy: {}", cumulativeSignalNoiseRatio, last4CumulativeEnergy);

            if (maxCumulativeSignalNoiseRatio < cumulativeSignalNoiseRatio) {
                maxCumulativeSignalNoiseRatio = cumulativeSignalNoiseRatio;
//                    log("\t setting maxCumulativeSignalNoiseRatio to {}", maxCumulativeSignalNoiseRatio);
            }

        }

        if (maxLast4CumulativeEnergy < last4CumulativeEnergy) {
            maxLast4CumulativeEnergy = last4CumulativeEnergy;
//                log("\t setting maxLast4CumulativeEnergy to {}", maxLast4CumulativeEnergy);
        }
//            if (silentCount != 0) {
//                log("\t Muting trigger. Count down to 0: {}", silentCount);
//                silentCount--;
//            } else if (last4CumulativeEnergy > ENERGY_THRESHOLD
//                && cumulativeSignalNoiseRatio > SIGNAL_NOISE_RATIO_THRESHOLD) {
//                pullTrigger();
//                silentCount = SILENT_COUNT;
//                log("\t Resetting silentCount to {}", silentCount);
//            }
        signalEvent.getCountDownLatch().countDown();
        log("calcPower done {} ", signalEvent);
    }

    @Override
    public double last10CumulativeEnergy(final long length){
        return last10Events.stream().limit(length).map(SignalEvent::getAmplitude)
            .map(dbl -> dbl * dbl).mapToDouble(i -> i).sum();
    }

    @Override
    public double getMaxLast4CumulativeEnergy() {
        return maxLast4CumulativeEnergy;
    }

    @Override
    public double getLast10CumulativeEnergy() {
        return last10CumulativeEnergy;
    }

    @Override
    public double getLast4CumulativeEnergy() {
        return last4CumulativeEnergy;
    }

    @Override
    public double getLast4AverageEnergy() {
        return last4AverageEnergy;
    }

    @Override
    public double getLast10AverageEnergy() {
        return last10AverageEnergy;
    }

    @Override
    public double getCumulativeSignalNoiseRatio() {
        return cumulativeSignalNoiseRatio;
    }

    private void log(final String msg, final Object... params) {
        final Object[] newParams = new Object[params.length + 1];
        System.arraycopy(params, 0, newParams, 1, params.length);
        newParams[0] = myFrequency;
        logger.debug("IEnergyCaptor[{}] " + msg, newParams);
    }

    @Override
    public int getFreq() {
        return myFrequency;
    }

    @Override
    public String toString() {
        return "EnergyCaptor{" +
            ", Freq=" + myFrequency +
            ", cumulativeSignalNoiseRatio=" + cumulativeSignalNoiseRatio +
            ", last4CumEnergy=" + last4CumulativeEnergy +
            ", last4AveEnergy=" + last4AverageEnergy +
            ", last10CumEnergy=" + last10CumulativeEnergy +
            ", last10AveEnergy=" + last10AverageEnergy +
            ", maxCumulativeSignalNoiseRatio=" + maxCumulativeSignalNoiseRatio +
            ", maxLast4CumulativeEnergy=" + maxLast4CumulativeEnergy +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnergyCaptor that = (EnergyCaptor) o;
        return myFrequency == that.myFrequency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(myFrequency);
    }
}
