package org.amd.dtmfDecoder.analyser;

import org.amd.dtmfDecoder.analyser.dtmf.DtmfMap;
import org.amd.dtmfDecoder.utils.DtmfUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author iyuvchenko 2018
 */
public interface EnergyCaptorFactory {
    static Map<Integer, IEnergyCaptor> create(final int spectrumWidth) {
        final ExecutorService executorService = Executors.newScheduledThreadPool(4);

        //With 8000 sampling rate and number of frequencies (spectrumWidth) distance between frequencies is:
        final int frequencyWidth = DtmfUtils.SAMPLE_RATE / 2 / spectrumWidth;

        final List<IEnergyCaptor> l = IntStream.range(1, spectrumWidth + 1).map(i -> i * frequencyWidth)
            .filter(DtmfMap.getAllTonesPredicate())
            .mapToObj(freq -> new EnergyCaptor(freq, executorService)).collect(Collectors.toList());

        final Map<Integer, IEnergyCaptor> map = new HashMap<>();
        l.forEach(captor-> map.put(captor.getFreq(), captor));

        return map;
    }
}
