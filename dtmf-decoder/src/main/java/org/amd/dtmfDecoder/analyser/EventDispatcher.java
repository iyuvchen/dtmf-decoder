package org.amd.dtmfDecoder.analyser;

import edu.princeton.cs.Complex;
import org.amd.dtmfDecoder.analyser.dtmf.DtmfMap;
import org.amd.dtmfDecoder.analyser.message.AccumulateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import static org.amd.dtmfDecoder.utils.DtmfUtils.SAMPLE_RATE;

/**
 * @author iyuvchenko 2018
 */
class EventDispatcher implements IEventDispatcher {
    private static final Logger logger = LoggerFactory.getLogger(EventDispatcher.class);
    private final Map<Integer, IEnergyCaptor> energyCaptors;
    private final int freqStep;
    private final IRecognizer recognizer;

    EventDispatcher(final IRecognizer recognizer, final int spectrumWidth) {
        this.recognizer = recognizer;
        this.energyCaptors = EnergyCaptorFactory.create(spectrumWidth);
        this.freqStep = (SAMPLE_RATE / 2) / (spectrumWidth);

        final Set<IEnergyCaptor> mainCaptors = new HashSet<>();
        final Set<IEnergyCaptor> auxCaptors = new HashSet<>();

        //todo use Collectors.groupingBy
        energyCaptors.values().forEach(captor-> {
            if (DtmfMap.getMainTonesPredicate().test(captor.getFreq())){
                mainCaptors.add(captor);
            }else if (DtmfMap.getAuxiliaryTonesPredicate().test(captor.getFreq())) {
                auxCaptors.add(captor);
            }
        });
        recognizer.setMainEnergyCaptors(mainCaptors);
        recognizer.setAuxEnergyCaptors(auxCaptors);
        recognizer.setAllEnergyCaptors(energyCaptors.values());

    }

    @Override
    public Collection<IEnergyCaptor> getEnergyCaptors() {
        return energyCaptors.values();
    }

    @Override
    public void dispatch(Complex[] yy) {
        logger.debug("Started dispatching events");
        final CountDownLatch countDownLatch = new CountDownLatch(energyCaptors.size());

        DtmfMap.ALL_TONES_SET.forEach(freq -> {
            final int index = freq / freqStep - 1;
            energyCaptors.get(freq).accumulate(new AccumulateEvent(freq, yy[index].re(), countDownLatch));
        });

        try {
            countDownLatch.await();
            recognizer.recognize();
        } catch (InterruptedException e) {
            logger.error("Error while waiting for count down", e);
            throw new RuntimeException(e);
        }

        logger.debug("Finished dispatching events");
    }
}