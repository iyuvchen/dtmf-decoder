package org.amd.dtmfDecoder.analyser;

import io.vavr.Tuple2;
import org.amd.dtmfDecoder.analyser.dtmf.DtmfEvent;
import org.amd.dtmfDecoder.analyser.dtmf.DtmfMap;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author iyuvchenko 2018
 */
public class FreqRecognizer implements IRecognizer {
    private static final Logger logger = LoggerFactory.getLogger(FreqRecognizer.class);

    private static final Comparator<IEnergyCaptor> cumulativeSignalNoiseRatioComparator = (o1, o2) ->
        Double.compare(o1.getCumulativeSignalNoiseRatio(), o2.getCumulativeSignalNoiseRatio());

    private static final Comparator<IEnergyCaptor> last4AverageEnergyComparator = (o1, o2) ->
        Double.compare(o1.getLast4AverageEnergy(), o2.getLast4AverageEnergy());

    private static final Comparator<Map.Entry<Integer, Long>> auxComparator = (entry1, entry2) ->
        Long.compare(entry1.getValue(), entry2.getValue());
    
    private Collection<IEnergyCaptor> allCaptors;
    private Collection<IEnergyCaptor> mainCaptors;
    private Collection<IEnergyCaptor> auxCaptors;

    private Map<Double, Integer> mainFreqCandidates = new HashMap<>();
    private Map<Double, Integer> auxFreqCandidates = new HashMap<>();
    private final Consumer<DtmfEvent> callback;
    private int counter = 1;
    private int muteCounter = 7;
    private boolean mainToneDetected = false;
    private int mainToneDetectedCounter = 0;
    private boolean muted = false;

    public FreqRecognizer(final Consumer<DtmfEvent> callback) {
        this.callback = callback;
    }
    
    private Tuple2<Double, Integer> getRatioAndFreq(final Collection<IEnergyCaptor> captors) {
        final List<IEnergyCaptor> sortedCaptors = captors.stream().sorted(last4AverageEnergyComparator).collect(Collectors.toList());
        final double averageEnergyOfLargest = sortedCaptors.get(3).getLast4AverageEnergy();
        final double averageEnergyOf3Smallest = Stream.of(sortedCaptors.get(0), sortedCaptors.get(1), sortedCaptors.get(2)).mapToDouble(IEnergyCaptor::getLast4AverageEnergy).sum() / 3;
        final double ratio = averageEnergyOf3Smallest == 0.0 ? 0
            : averageEnergyOfLargest / averageEnergyOf3Smallest;

        return new Tuple2<>(ratio, sortedCaptors.get(3).getFreq());
    }

    private void fireDecision() {
        //we got enough data to make final decision about aux freq
        final Optional<Integer> mainFreq = mainFreqCandidates.entrySet().stream()
            .map(Map.Entry::getKey)
            .max(Double::compare)
            .map(max ->
                mainFreqCandidates.get(max));

        final Optional<Integer> auxFreq = getRefinedAuxFreq(auxFreqCandidates);

        if (mainFreq.isPresent() && auxFreq.isPresent()) {
            DtmfMap.freqToDtmfEvent(new int[]{mainFreq.get(), auxFreq.get()}).ifPresent(callback);
            DtmfMap.freqToDtmfEvent(new int[]{mainFreq.get(), auxFreq.get()}).ifPresent(dtmfEvent -> {
                    if (dtmfEvent.getValue().equals("7")) {
                        auxFreqCandidates.size();
                        boolean v=true;
                    }}
                );
            logger.debug("Firing dtmf {} {}", mainFreq.get(), auxFreq.get());
        }
    }

    private Optional<Integer> getRefinedAuxFreq(Map<Double,Integer> auxFreqCandidates){
        final Optional<Integer> auxFreqByMaxValue = auxFreqCandidates.entrySet().stream()
            .map(Map.Entry::getKey)
            .max(Double::compare)
            .map(max ->
                auxFreqCandidates.get(max));

        final Map<Integer,Long> auxFreqGroupBy = auxFreqCandidates.values().stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        final Optional<Integer> auxFreqByMaxCount =  auxFreqGroupBy.entrySet().stream().max(auxComparator).map(Map.Entry::getKey);
        if(auxFreqByMaxValue.isPresent()&& auxFreqByMaxCount.isPresent()){
            if(!auxFreqByMaxValue.get().equals(auxFreqByMaxCount.get())){
                logger.debug("auxFreqByMaxValue and auxFreqByMaxCount are different {} {}", auxFreqByMaxValue.get(), auxFreqByMaxCount.get());
                long byMaxVal = auxFreqGroupBy.get(auxFreqByMaxValue.get());
                long byMaxCount = auxFreqGroupBy.get(auxFreqByMaxCount.get());
                if(byMaxVal== byMaxCount){
                    return auxFreqByMaxValue;
                }
            }
        }

        return auxFreqByMaxCount;
    }

    private void checkCaptors() {
        if (allCaptors == null || mainCaptors == null || auxCaptors == null) {
            logger.error("Captors are not set.");
            throw new RuntimeException("Captors are not set.");
        }
    }

    private Tuple2<Double, Integer> getAuxRatioAndSetCandidates(final Tuple2<Double, Integer> mainRatioAndFreq){
        final Tuple2<Double, Integer> auxRatioAndFreq = getRatioAndFreq(auxCaptors);

        mainFreqCandidates.put(mainRatioAndFreq._1, mainRatioAndFreq._2);
        auxFreqCandidates.put(auxRatioAndFreq._1, auxRatioAndFreq._2);
        return auxRatioAndFreq;
    }

    @Override
    public void recognize() {
        checkCaptors();

//        if(counter * DtmfUtils.BLOCK_SIZE / 8>1500)
//        {
//            boolean b = true;
//        }

        if(muted){
            logger.debug("{}ms recognize: muted {}", counter * DtmfUtils.BLOCK_SIZE / 8, muteCounter);
            if(muteCounter == 0){
                muted = false;
            }
            muteCounter--;
            counter++;
            return;
        }

        final Tuple2<Double, Integer> mainRatioAndFreq = getRatioAndFreq(mainCaptors);
        if (mainRatioAndFreq._1 > 18.0) {
            mainToneDetected = true;
            mainToneDetectedCounter++;

            final Tuple2<Double, Integer> auxRatioAndFreq = getAuxRatioAndSetCandidates(mainRatioAndFreq);

            logger.debug("{}ms recognize: mainRatioAndFreq:{} auxRatioAndFreq:{} mainToneDetectedCounter: {}",
                counter * DtmfUtils.BLOCK_SIZE / 8,
                mainRatioAndFreq,
                auxRatioAndFreq,
                mainToneDetectedCounter);

        } else {
            if (mainToneDetected) {
                mainToneDetected = false;
                muted = true;
                muteCounter = 7;
                if (mainToneDetectedCounter >= 5) {
                    fireDecision();
                }
                mainFreqCandidates.clear();
                auxFreqCandidates.clear();
                mainToneDetectedCounter = 0;
            }

            logger.debug("{}ms recognize: mainRatioAndFreq:{} ",
                counter * DtmfUtils.BLOCK_SIZE / 8,
                mainRatioAndFreq
            );
        }
        counter++;
    }


    @Override
    public void setAllEnergyCaptors(Collection<IEnergyCaptor> energyCaptors) {
        this.allCaptors = energyCaptors;
    }

    @Override
    public void setMainEnergyCaptors(Collection<IEnergyCaptor> energyCaptors) {
        this.mainCaptors = energyCaptors;
    }

    @Override
    public void setAuxEnergyCaptors(Collection<IEnergyCaptor> energyCaptors) {
        this.auxCaptors = energyCaptors;
    }
}
