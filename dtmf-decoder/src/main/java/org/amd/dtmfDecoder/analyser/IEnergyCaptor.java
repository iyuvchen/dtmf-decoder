package org.amd.dtmfDecoder.analyser;

import org.amd.dtmfDecoder.analyser.message.AccumulateEvent;

/**
 * @author iyuvchenko 2018
 */
public interface IEnergyCaptor {


    /**
     * Every event contributes to energy level.
     * @param signalEvent
     */
    void accumulate(AccumulateEvent signalEvent);

    double last10CumulativeEnergy(long length);

    double getMaxLast4CumulativeEnergy();

    double getLast10CumulativeEnergy();

    double getLast4CumulativeEnergy();

    double getLast4AverageEnergy();

    double getLast10AverageEnergy();

    double getCumulativeSignalNoiseRatio();

    int getFreq();
}
