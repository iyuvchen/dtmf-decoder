package org.amd.dtmfDecoder.analyser;

import edu.princeton.cs.Complex;

import java.util.Collection;


/**
 * @author iyuvchenko 2018
 */
public interface IEventDispatcher {
    Collection<IEnergyCaptor> getEnergyCaptors();
    void dispatch(Complex[] yy);

    static IEventDispatcher createDefault(final IRecognizer recognizer, final int blockSize){
        return new EventDispatcher(recognizer, blockSize);
    }

}
