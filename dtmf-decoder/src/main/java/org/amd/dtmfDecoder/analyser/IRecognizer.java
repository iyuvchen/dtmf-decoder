package org.amd.dtmfDecoder.analyser;

import java.util.Collection;

/**
 * @author iyuvchenko 2018
 */
public interface IRecognizer {
    void recognize();
    void setAllEnergyCaptors(Collection<IEnergyCaptor> energyCaptors);
    void setMainEnergyCaptors(Collection<IEnergyCaptor> energyCaptors);
    void setAuxEnergyCaptors(Collection<IEnergyCaptor> energyCaptors);
}
