package org.amd.dtmfDecoder.analyser;

import edu.princeton.cs.Complex;
import edu.princeton.cs.FFT;
import io.vavr.control.Try;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.amd.dtmfDecoder.utils.TxtFileWriterImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import static org.amd.dtmfDecoder.utils.DtmfUtils.BLOCK_SIZE;
import static org.amd.dtmfDecoder.utils.DtmfUtils.toDoubleList;

/**
 * @author iyuvchenko 2018
 */
public class SpectrumAnalyser {
    private static final Logger logger = LoggerFactory.getLogger(SpectrumAnalyser.class);

    private final BlockingQueue<short[]> blockingQueue = new LinkedBlockingQueue<>();
    private final TxtFileWriterImpl fileWriter = new TxtFileWriterImpl("python/double.csv");
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final IEventDispatcher eventDispatcher;
    private short[] leftOver = {};

    public SpectrumAnalyser(final IEventDispatcher eventDispatcher) {
        this.eventDispatcher = eventDispatcher;
        executorService.submit(() -> {
            try {
//                Thread.sleep(1000);
                while (true) {
                    final short[] data = blockingQueue.take();
                    if (data != null) {
                        doAnalysisAndQueue(data);
                    }

                    if (Thread.interrupted())
                        break;
                }
            } catch (InterruptedException e) {
                logger.error("ERROR!", e.getMessage());
                throw new RuntimeException(e);
            }
        });
        logger.debug("block duration {} s | frequency step {} Hz", (1 / 8000.0) * BLOCK_SIZE, 4000.0 / BLOCK_SIZE * 2);
    }

    public void stop() {
        executorService.shutdownNow();
    }

    private synchronized void doAnalysisAndQueue(short[] sublist) {
        logger.debug("Dequeued and started FFT");
        final Complex[] data = DtmfUtils.toComplex(sublist);
        Complex[] yy = FFT.fft(data);
//        logger.debug("SpectrumAnalyser finished doAnalysisAndQueue");
        Try.run(() -> eventDispatcher.dispatch(Arrays.copyOfRange(yy, 0, sublist.length / 2)))
            .onFailure(t -> logger.error("Error dispatching ", t));

        fileWriter.append(toDoubleList(yy, false).subList(0, BLOCK_SIZE / 2));
        logger.debug("Appended FFT data to file {}", "double.csv");
    }

    public void submitForAnalysis(final short[] shorts) {

        leftOver = DtmfUtils.joinTwoArrays(leftOver, shorts);

        final short[] data = leftOver;

        int counter = 0;
        int blockSize = BLOCK_SIZE; //(1/8000)*128 = 0.016 ms block duration, frequency step: 62.5Hz

        while (true) {
            final int toIndex = counter * BLOCK_SIZE + BLOCK_SIZE;
            final int fromIndex = counter * BLOCK_SIZE;

            if (toIndex > data.length) {
                leftOver = Arrays.copyOfRange(leftOver, fromIndex, leftOver.length);
                break;
            }

            final short[] inProgress = Arrays.copyOfRange(leftOver, fromIndex, toIndex);

            blockingQueue.add(inProgress);

            logger.debug("Queued {} samples, queue size - {}", toIndex - (counter * BLOCK_SIZE), blockingQueue.size());
            counter++;
        }
    }

    IEventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    short[] getLeftOver() {
        return leftOver;
    }
}
