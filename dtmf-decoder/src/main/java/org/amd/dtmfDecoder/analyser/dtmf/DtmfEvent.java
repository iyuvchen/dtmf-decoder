package org.amd.dtmfDecoder.analyser.dtmf;

/**
 * @author iyuvchenko 2018
 */
public class DtmfEvent {

    private final String value;

    public DtmfEvent(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "DtmfEvent{" +
            "value='" + value + '\'' +
            '}';
    }
}
