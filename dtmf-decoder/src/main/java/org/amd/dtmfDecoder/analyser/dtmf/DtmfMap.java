package org.amd.dtmfDecoder.analyser.dtmf;

import java.util.*;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

/**
 * @author iyuvchenko 2018
 */
public class DtmfMap {
//        freq map from http://onlinetonegenerator.com/dtmf.html
    /**
     * {@link DtmfMap#MAIN_TONES_MAP}
     * <p>
     * Contains official tones from http://onlinetonegenerator.com/dtmf.html
     * where key is the official freq and value of the map is the freq obtained from FFT algorithm used in this project UTIL
     */

    public final static Map<Integer, Integer> MAIN_TONES_MAP = new HashMap<>();
    public final static Set<Integer> MAIN_TONES_SET;
    public final static Map<Integer, Integer> AUXILIARY_TONES_MAP = new HashMap<>();
    public final static Set<Integer> AUXILIARY_TONES_SET;
    public final static Set<Integer> ALL_TONES_SET = new HashSet<>();

    public final static Map<int[], String> DTMFS = new HashMap<>();

    public final static int amd_1240 = 1240;
    public final static int amd_1364 = 1364;
    public final static int amd_1488 = 1488;
    public final static int amd_1674 = 1674;

    public final static int amd_744 = 744;
    public final static int amd_806 = 806;
    public final static int amd_868 = 868;
    public final static int amd_992 = 992;

    static {

        final int official_1209 = 1209;
        final int official_1336 = 1336;
        final int official_1477 = 1477;
        final int official_1633 = 1633;

        MAIN_TONES_MAP.put(amd_1240, official_1209);
        MAIN_TONES_MAP.put(amd_1364, official_1336);
        MAIN_TONES_MAP.put(amd_1488, official_1477);
        MAIN_TONES_MAP.put(amd_1674, official_1633);

        MAIN_TONES_SET = MAIN_TONES_MAP.keySet();

        final int official_697 = 697;
        final int official_770 = 770;
        final int official_852 = 852;
        final int official_941 = 941;

        AUXILIARY_TONES_MAP.put(amd_744, official_697);
        AUXILIARY_TONES_MAP.put(amd_806, official_770);
        AUXILIARY_TONES_MAP.put(amd_868, official_852);
        AUXILIARY_TONES_MAP.put(amd_992, official_941);

        AUXILIARY_TONES_SET = AUXILIARY_TONES_MAP.keySet();

        ALL_TONES_SET.addAll(MAIN_TONES_SET);
        ALL_TONES_SET.addAll(AUXILIARY_TONES_SET);

        DTMFS.put(new int[]{amd_1240, amd_744}, "1");
        DTMFS.put(new int[]{amd_1364, amd_744}, "2");
        DTMFS.put(new int[]{amd_1488, amd_744}, "3");

        DTMFS.put(new int[]{amd_1240, amd_806}, "4");
        DTMFS.put(new int[]{amd_1364, amd_806}, "5");
        DTMFS.put(new int[]{amd_1488, amd_806}, "6");

        DTMFS.put(new int[]{amd_1240, amd_868}, "7");
        DTMFS.put(new int[]{amd_1364, amd_868}, "8");
        DTMFS.put(new int[]{amd_1488, amd_868}, "9");

        DTMFS.put(new int[]{amd_1240, amd_992}, "*");
        DTMFS.put(new int[]{amd_1364, amd_992}, "0");
        DTMFS.put(new int[]{amd_1488, amd_992}, "#");
    }

    public static Predicate<Integer> getMainTonesPredicate() {
        return MAIN_TONES_SET::contains;
    }

    public static IntPredicate getAllTonesPredicate() {
        return ALL_TONES_SET::contains;
    }

    public static Predicate<Integer> getAuxiliaryTonesPredicate() {
        return AUXILIARY_TONES_SET::contains;
    }

    public static Optional<DtmfEvent> freqToDtmfEvent(final int [] fs){
        for (Map.Entry<int[], String> entry : DTMFS.entrySet()){
            if (Arrays.equals(entry.getKey(), fs)){
                return Optional.of(new DtmfEvent(entry.getValue()));
            }
        }
        return Optional.empty();
    }
}
