package org.amd.dtmfDecoder.analyser.message;

import java.util.concurrent.CountDownLatch;

/**
 * @author iyuvchenko 2018
 */
public class AccumulateEvent {
    private final int frequency;
    private final double amplitude;
    private final CountDownLatch countDownLatch;

    public AccumulateEvent(final int frequency, final double amplitude, final CountDownLatch countDownLatch) {
        this.frequency = frequency;
        this.countDownLatch = countDownLatch;

        if (amplitude<0){
            this.amplitude = amplitude*-1.;
        }else {
            this.amplitude = amplitude;
        }
    }

    public SignalEvent toSignalEvent() {
        return new SignalEvent(frequency, amplitude, countDownLatch);
    }

    @Override
    public String toString() {
        return "SignalEvent{" +
            "frequency=" + frequency +
            ", amplitude=" + amplitude +
            '}';
    }
}
