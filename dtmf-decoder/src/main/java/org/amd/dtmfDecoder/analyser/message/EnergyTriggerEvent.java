package org.amd.dtmfDecoder.analyser.message;

/**
 * @author iyuvchenko 2018
 */
public class EnergyTriggerEvent {
    private final int frequency;
    private final double signalNoiseRatio;
    private final double last4CumulativeEnergy;

    public EnergyTriggerEvent(final int frequency, final double signalNoiseRatio, final double last4CumulativeEnergy ) {
        this.frequency = frequency;
        this.signalNoiseRatio = signalNoiseRatio;
        this.last4CumulativeEnergy = last4CumulativeEnergy;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getSignalNoiseRatio() {
        return signalNoiseRatio;
    }


    @Override
    public String toString() {
        return "EnergyTriggerEvent{" +
            "frequency=" + frequency +
            ", signalNoiseRatio=" + signalNoiseRatio +
            ", last4CumulativeEnergy=" + last4CumulativeEnergy +
            '}';
    }
}
