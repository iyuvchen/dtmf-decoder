package org.amd.dtmfDecoder.analyser.message;

import org.amd.dtmfDecoder.analyser.dtmf.DtmfMap;

import java.util.concurrent.CountDownLatch;

/**
 * @author iyuvchenko 2018
 */
public class SignalEvent {
    private final int frequency;
    private final double amplitude;
    private final CountDownLatch countDownLatch;

    public SignalEvent(final int frequency, final double amplitude, final CountDownLatch countDownLatch) {
        this.frequency = frequency;

        switch (frequency){
            case DtmfMap.amd_992:
                this.amplitude = amplitude * 0.5;
                break;

            case DtmfMap.amd_868:
                this.amplitude = amplitude * 0.7;
                break;

            case DtmfMap.amd_806:
                this.amplitude = amplitude * 0.9;
                break;

            default:
                this.amplitude = amplitude*1.2;
        }

        this.countDownLatch = countDownLatch;
    }

    public int getFrequency() {
        return frequency;
    }

    public double getAmplitude() {
        return amplitude;
    }

    public CountDownLatch getCountDownLatch() {
        return countDownLatch;
    }

    @Override
    public String toString() {
        return "SignalEvent{" +
            "frequency=" + frequency +
            ", amplitude=" + amplitude +
            '}';
    }
}
