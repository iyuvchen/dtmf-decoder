package org.amd.dtmfDecoder.micCapture;

import org.amd.dtmfDecoder.utils.AudioFileWriter;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.*;
import java.io.File;
import java.util.function.Consumer;

/**
credits:
https://gist.github.com/fedor-rusak/2294168
*/
public class MicReader implements Runnable{
    private static final Logger logger = LoggerFactory.getLogger(MicReader.class);

    private final TargetDataLine sourceLine;
    private Consumer<short[]> callback;
    private volatile boolean stop = false;

    public MicReader(final Consumer<short[]> callback) {
        try {
            final AudioFormat format = new AudioFormat(8000, 16, 1, true, true);
            final DataLine.Info sourceInfo = new DataLine.Info(TargetDataLine.class, format);

            sourceLine = (TargetDataLine) AudioSystem.getLine(sourceInfo);
            sourceLine.open(format);
        } catch (final LineUnavailableException e) {
            throw new RuntimeException(e);
        }
        this.callback = callback;
    }

    private void start() {
        sourceLine.start();
        int numBytesRead;
        byte[] inputData = new byte[sourceLine.getBufferSize() / 5];

        while (true) {
            numBytesRead = sourceLine.read(inputData, 0, inputData.length);

            if (numBytesRead == -1 || stop) {
                logger.debug("Exiting from mic reading!!!!!!!");
                break;
            }
            callback.accept(DtmfUtils.byteArrToShort(inputData));
        }
    }

    @Override
    public void run() {
        start();
    }

    public void shutdown(){
        stop = true;
    }

    private static short[] data = {};

    public static void main(String [] a) throws InterruptedException {

        final Consumer<short[]> callback = arr -> {
            logger.debug("got {} samples from MixReader", arr.length);
            data = DtmfUtils.joinTwoArrays(data,arr);
        };

        MicReader micReader = new MicReader(callback);
        Thread t1 = new Thread(micReader);
        t1.start();
        Thread.sleep(5000);
        micReader.shutdown();

        new AudioFileWriter(data,new File("fromMic.au")).write();

    }
}