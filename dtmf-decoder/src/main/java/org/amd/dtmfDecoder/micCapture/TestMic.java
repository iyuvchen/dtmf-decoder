package org.amd.dtmfDecoder.micCapture;

import javax.sound.sampled.*;
/**
credits:
https://gist.github.com/fedor-rusak/2294168
*/
public class TestMic {

    public static void main(String[] args) {
        AudioFormat format = new AudioFormat(8000, 16, 1, true, true);

        DataLine.Info targetInfo = new DataLine.Info(TargetDataLine.class, format);
        DataLine.Info sourceInfo = new DataLine.Info(SourceDataLine.class, format);

        try {
            TargetDataLine sourceLine = (TargetDataLine) AudioSystem.getLine(targetInfo);
            sourceLine.open(format);
            sourceLine.start();

            SourceDataLine outputLine = (SourceDataLine) AudioSystem.getLine(sourceInfo);
            outputLine.open(format);
            outputLine.start();

            int numBytesRead;
            byte[] inputData = new byte[sourceLine.getBufferSize() / 5];

            while (true) {
                numBytesRead = sourceLine.read(inputData, 0, inputData.length);

                if (numBytesRead == -1) break;

                outputLine.write(inputData, 0, numBytesRead);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

}