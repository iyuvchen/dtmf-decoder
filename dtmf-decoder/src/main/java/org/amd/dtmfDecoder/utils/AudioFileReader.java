package org.amd.dtmfDecoder.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

import static org.amd.dtmfDecoder.utils.DtmfUtils.byteArrToShort;

/**
 * Created by iyuvchenko on 4/30/18.
 * Twilio 2018
 */
public class AudioFileReader {
    private static final Logger logger = LoggerFactory.getLogger(AudioFileReader.class);

    private final FileInputStream inputStream;
    private final int headerLength = 6 * 4; //au auFileHeader is six 32-bit words
    private final byte[] auFileHeader = new byte[headerLength];
    private final int dataOffset;
    private final int dataSize;
    private final int encoding;
    private final int sampleRate;
    private final int channels;
    private final short[] body;

    public AudioFileReader(final String fname) {
        try {
            this.inputStream = new FileInputStream(fname);
            final int length = inputStream.available();
            final byte[] allData = new byte[length];
            final int result = inputStream.read(allData, 0, length);

            if (result == -1 || result != length) {
                throw new IOException("unable to read au file " + inputStream.getChannel().toString());
            }

            System.arraycopy(allData, 0, auFileHeader, 0, headerLength);

            if (!(auFileHeader[0] == 46 &&
                auFileHeader[1] == 115 &&
                auFileHeader[2] == 110 &&
                auFileHeader[3] == 100)) {
                throw new IOException("not au auFileHeader: .snd");
            }

            this.dataOffset = getWord32(auFileHeader, 1);

            if (dataOffset != 24) {
                throw new RuntimeException("This program can't process this dataOffset  " + dataOffset);
            }

            this.dataSize = getWord32(auFileHeader, 2) == -1 ? (length - headerLength) : getWord32(auFileHeader, 2);

            if (dataSize != length - headerLength) {
                logger.warn("dataSize {} is incorrect, content length: {}", dataSize, length - headerLength);
            }

            this.encoding = getWord32(auFileHeader, 3);
            sampleRate = getWord32(auFileHeader, 4);
            channels = getWord32(auFileHeader, 5);

            body = byteArrToShort(Arrays.copyOfRange(allData, headerLength, allData.length));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static int getWord32(final byte[] auFileHeader, final int wordCount) {
        final int offset = wordCount * 4;
        final byte byte_1 = auFileHeader[offset];
        final byte byte_2 = auFileHeader[offset + 1];
        final byte byte_3 = auFileHeader[offset + 2];
        final byte byte_4 = auFileHeader[offset + 3];

        final ByteBuffer bb = ByteBuffer.wrap(new byte[]{byte_1, byte_2, byte_3, byte_4}).order(ByteOrder.BIG_ENDIAN);
        return bb.getInt();
    }

    public void dumpToFileAsInt(final String fname) {
        try {
            final File file = new File(fname);

            if (file.delete()) {
                System.out.println("File deleted successfully " + file.getName());
            } else {
                System.out.println("Failed to delete the file" + file.getName());
            }

            BufferedWriter outputWriter = new BufferedWriter(new java.io.FileWriter(file));

            for (int i = 0; i < this.body.length; i++) {

                outputWriter.write(Integer.toString(this.body[i]));

                outputWriter.newLine();
            }
            outputWriter.flush();
            outputWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public int getDataOffset() {
        return dataOffset;
    }

    public int getDataSize() {
        return dataSize;
    }

    public int getEncoding() {
        return encoding;
    }

    public int getSampleRate() {
        return sampleRate;
    }

    public int getChannels() {
        return channels;
    }

    public short[] getBody() {
        if(sampleRate!=8000){
            throw new RuntimeException("sampleRate!=8000 :"+sampleRate );
        }
        return body;
    }

    public short[] getBodyFragment(final int fromMs, final int toMs) {
        return DtmfUtils.getBodyFragment(body,fromMs,toMs);
    }

}
