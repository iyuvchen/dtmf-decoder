package org.amd.dtmfDecoder.utils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * @author iyuvchenko 2018
 */
public class AudioFileWriter {
    //                                       .    s    n    d |  data offset | data size    | encoding  | sample rate | channels |
    private final static byte[] AUHEADER = {46, 115, 110, 100, 0, 0, 0, 24, 0, 45, -96, 0, 0, 0, 0, 3, 0, 0, 31, 64, 0, 0, 0, 1};
    //only reference             ^^^

    private final static byte[] SND_HEADER = {46, 115, 110, 100};
    private final static byte[] DATA_OFFSET = {0, 0, 0, 24};
    private final static byte[] ENCODING = {0, 0, 0, 3};
    private final static byte[] SAMPLE_RATE = {0, 0, 31, 64};
    private final static byte[] CHANNELS = {0, 0, 0, 1};
    private final static int HEADER_SIZE = 24;

    private final ByteBuffer head = ByteBuffer.allocate(HEADER_SIZE);

    private final short[] shortArrayList;
    private final File outputAuFile;

    public AudioFileWriter(final File dataFile, final File outputAuFile) {
        try (final BufferedReader br = new BufferedReader(new FileReader(dataFile))) {
            this.shortArrayList = new short[(int) dataFile.length() / 2];
            String line;
            int count=0;
            while ((line = br.readLine()) != null) {
                final short x = Short.parseShort(line);
                shortArrayList[count]=x;
                count++;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.outputAuFile = outputAuFile;
    }


    public AudioFileWriter(final short[] body, final File outputAuFile) {
        this.shortArrayList = (body);
        this.outputAuFile = outputAuFile;
    }

    public void write() {
        if (outputAuFile.delete()) {
            System.out.println("File deleted successfully " + outputAuFile.getName());
        }

        try (final FileOutputStream fos = new FileOutputStream(outputAuFile)) {
            final ByteBuffer data = ByteBuffer.allocate(shortArrayList.length * 2 + HEADER_SIZE);
            data.order(ByteOrder.BIG_ENDIAN);

            head.put(SND_HEADER);
            head.put(DATA_OFFSET);
            head.putInt(shortArrayList.length * 2);
            head.put(ENCODING);
            head.put(SAMPLE_RATE);
            head.put(CHANNELS);
            head.order(ByteOrder.BIG_ENDIAN);

            data.put(head.array());
            for(short s :shortArrayList){
                data.putShort(s);
            }

            fos.write(data.array());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void dumpToFileAsInt(final String fname) {
        try {
            final File file = new File(fname);

            if (file.delete()) {
                System.out.println("File deleted successfully " + file.getName());
            } else {
                System.out.println("Failed to delete the file" + file.getName());
            }

            BufferedWriter outputWriter = new BufferedWriter(new java.io.FileWriter(file));

            for (final Short s: shortArrayList) {

                outputWriter.write(Integer.toString(s));

                outputWriter.newLine();
            }
            outputWriter.flush();
            outputWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new AudioFileWriter(new File("pic.dat"), new File("output.au")).write();
    }
}
