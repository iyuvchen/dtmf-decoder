package org.amd.dtmfDecoder.utils;

import edu.princeton.cs.Complex;
import org.amd.dtmfDecoder.RealTimeAnalyser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.nio.charset.StandardCharsets.US_ASCII;


/**
 * @author iyuvchenko 2018
 */
public class DtmfUtils {
    public static final int BLOCK_SIZE = 128;
    public static final int SAMPLE_RATE = 8000;

    public static Complex[] toComplex(final short[] data) {
        final int length = data.length;
        final Complex[] result = new Complex[length];
        for (int i = 0; i < length; i++) {
            final double d = data[i];
            result[i] = new Complex(d, 0);
        }

        return result;
    }

    public static short[] toShortList(final Complex[] data) {
        final int length = data.length;
        final short[] result = new short[length];
        int count = 0;
        for (Complex aData : data) {
            final short s;

            if (aData.re() > Short.MAX_VALUE)
                s = Short.MAX_VALUE;
            else if (aData.re() < Short.MIN_VALUE)
                s = Short.MIN_VALUE;
            else s = (short) aData.re();

            result[count] = s;
            count++;
        }

        return result;
    }

    public static List<Double> toDoubleList(final Complex[] data, final boolean raw) {
        final int length = data.length;
        final List<Double> result = new ArrayList<>(length);
        for (Complex aData : data) {
            if (raw) {
                result.add(aData.re());
            } else if (aData.re() < 0) {
                result.add(aData.re() * -1L);
            } else {
                result.add(aData.re());
            }
        }

        return result;
    }

    public static short[] joinTwoArrays(final short[] arr1, final short[] arr2) {
        final short[] temp = new short[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, temp, 0, arr1.length);
        System.arraycopy(arr2, 0, temp, arr1.length, arr2.length);
        return temp;
    }

    public static byte[] shortToByteArr(short x) {
        final byte[] ret = new byte[2];
        ret[0] = (byte) ((x >> 8) & 0xFF);
        ret[1] = (byte) (x & 0xFF);
        return ret;
    }

    public static short[] byteArrToShort(final byte[] data) {
        final short[] result = new short[data.length / 2];
        for (int offSet = 0; offSet < data.length / 2; offSet++) {
            result[offSet] = getShort(data, offSet * 2);
        }
        return result;
    }

    public static short getShort(final byte[] auFileHeader, final int offSet) {
        final byte byte_1 = auFileHeader[offSet];
        final byte byte_2 = auFileHeader[offSet + 1];

        final ByteBuffer bb = ByteBuffer.wrap(new byte[]{byte_1, byte_2}).order(ByteOrder.BIG_ENDIAN);
        final short result = bb.getShort();
        return result;
    }

    public static short[] getBodyFragment(final short[] body, final int fromMs, final int toMs) {
        if (fromMs >= toMs) {
            throw new RuntimeException("From is later than to. from :" + fromMs + " to: " + toMs);
        }
        final int fromIndex = (SAMPLE_RATE / 1000) * fromMs;
        final int toIndex = (SAMPLE_RATE / 1000) * toMs + 1;
        short[] res = Arrays.copyOfRange(body, fromIndex, toIndex);
        short[] res2 = Arrays.copyOfRange(body, fromIndex, toIndex);
        return res;
    }

    public static String readLogo() {
        final InputStream is = RealTimeAnalyser.class.getResourceAsStream("/logo.txt");
        try {
            byte[] bytes = new byte[is.available()];
            is.read(bytes);
            final String ss = new String(bytes, US_ASCII);
            return ss;
        } catch (IOException ignored) {

        }
        return "";
    }
}
