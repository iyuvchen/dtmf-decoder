package org.amd.dtmfDecoder.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iyuvchenko 2018
 */
public class Mixer {
    private final List<Short> oneThousand;
    private final List<Short> twoThousands;

    public Mixer(final List<Short> oneThousand, final List<Short> twoThousands) {
        this.oneThousand = oneThousand;
        this.twoThousands = twoThousands;
    }

    public List<Short> doMix() {
        int length = oneThousand.size() > twoThousands.size() ? oneThousand.size() : twoThousands.size();
        final List<Short> result = new ArrayList<>(length);

        for (int i = 0; i < length; i++) {
            final Short one = getData(oneThousand, i);
            final Short two = getData(twoThousands, i);

            final short r;
            if (one != null && two != null) {
                r = (short) ((one + two) / 2);
            } else {
                r = one != null ? one : two;
            }

            result.add(r);
        }

        return result;
    }

    private static Short getData(final List<Short> data, int index) {
        if (index + 1 <= data.size()){
            return data.get(index);
        }
        else return null;
    }
}
