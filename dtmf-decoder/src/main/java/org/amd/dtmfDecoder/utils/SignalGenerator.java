package org.amd.dtmfDecoder.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author iyuvchenko 2018
 */
public class SignalGenerator {

    private static final double SAMPLING_RATE = 8000;

    public static short[] generateDuration(int durationMs, final int freq, double amplitude) {
        final int number_of_samples = 8000 / 1000 * durationMs;
        return generateNumberOfSamples(number_of_samples, freq, amplitude);
    }

    public static short[] generateNumberOfSamples(final int number_of_samples, final int freq, double amplitude) {

        final short[] samples = new short[(number_of_samples)];
        final double piX2 = Math.PI * 2;

        for (int i = 0; i < number_of_samples; i++) {
            final double d = amplitude * Math.cos((i * piX2 * freq) / SAMPLING_RATE);
            samples[i] = (short) (d * 32000);
        }
        return samples;
    }

    public static void main(String[] a) {
        final int number_of_samples = 40000;
        final short[] data = generateNumberOfSamples(number_of_samples, 1000, 0.5);
        SignalGenerator.dumpToFileAsInt("pic.dat", data);
        new AudioFileWriter(data, new File("cos.au")).write();

    }

    public static void dumpToFileAsInt(final String fName, final short []samples) {
        try {
            final File file = new File(fName);

            if (file.delete()) {
                System.out.println("File deleted successfully " + file.getName());
            } else {
                System.out.println("Failed to delete the file" + file.getName());
            }

            BufferedWriter outputWriter = new BufferedWriter(new FileWriter(file));

            for (short sample : samples) {
                outputWriter.write(Short.toString(sample));
                outputWriter.newLine();
            }

            outputWriter.flush();
            outputWriter.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
