package org.amd.dtmfDecoder.utils;

import java.util.List;

/**
 * @author iyuvchenko 2018
 */
public interface TxtFileWriter {
    void append(final List<Double> data);
}
