package org.amd.dtmfDecoder.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author iyuvchenko 2018
 */
public class TxtFileWriterImpl implements TxtFileWriter {
    private static final Logger logger = LoggerFactory.getLogger(TxtFileWriterImpl.class);
    private final PrintWriter pw;

    public TxtFileWriterImpl(final String textFile) {
        if (new File(textFile).delete()) {
           logger.debug("File deleted successfully " + textFile);
        } else {
            logger.debug("Failed to delete the file " + textFile);
        }
        final FileWriter writer;
        try {
            writer = new FileWriter(textFile);
        } catch (IOException e) {
            throw new RuntimeException("Problem with " + textFile, e);
        }
        pw = new PrintWriter(writer, true);
    }

    @Override
    public void append(final List<Double> data) {
        pw.print(String.join(",", data.stream().map(Object::toString).collect(Collectors.toList())) + "\n");
        pw.flush();
    }
}
