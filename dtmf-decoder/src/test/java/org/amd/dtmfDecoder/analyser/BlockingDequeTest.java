package org.amd.dtmfDecoder.analyser;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import static junit.framework.TestCase.assertTrue;

/**
 * @author iyuvchenko 2018
 */
public class BlockingDequeTest {
    private BlockingDeque<Integer> blockingDeque;

    @Before
    public void setUp() throws Exception {
        blockingDeque = new LinkedBlockingDeque<>(1);
    }

    @Test
    public void add() throws InterruptedException {
        blockingDeque.putLast(2);
        assertTrue(2==blockingDeque.poll());
        blockingDeque.putLast(3);
        assertTrue(3 == blockingDeque.poll());
    }
}
