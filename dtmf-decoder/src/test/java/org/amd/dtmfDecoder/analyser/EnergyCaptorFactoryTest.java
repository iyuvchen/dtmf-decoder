package org.amd.dtmfDecoder.analyser;

import org.amd.dtmfDecoder.analyser.message.AccumulateEvent;
import org.amd.dtmfDecoder.analyser.message.EnergyTriggerEvent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.times;

/**
 * @author iyuvchenko 2018
 */
public class EnergyCaptorFactoryTest {
    private static final Logger logger = LoggerFactory.getLogger(EnergyCaptorFactoryTest.class);
    private IEnergyCaptor energyCaptor;
    private final static int FREQ = 744;
    private CountDownLatch countDownLatch;

    @Mock
    private Consumer<EnergyTriggerEvent> callback;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public TestName name = new TestName();

    @Before
    public void setUp() throws InterruptedException {
        countDownLatch = new CountDownLatch(1000);
        energyCaptor = EnergyCaptorFactory.create(128).get(FREQ);

        logger.debug("-----------Preparing to run {}", name.getMethodName());
//        Thread.sleep(1000);
        logger.debug("-----------Running {}", name.getMethodName());
    }

    @Test
    public void getLast20CumulativeEnergy() throws InterruptedException {
        double d = 246134.08356462448;
        final double i = 2000.0;
        IntStream.range(0, 11).forEach(k -> energyCaptor.accumulate(new AccumulateEvent(FREQ, i, countDownLatch)));
        Thread.sleep(30);
        double last10CumulativeEnergy = 6 * i * 1.2 * i * 1.2;
        assertEquals(last10CumulativeEnergy, energyCaptor.getLast10CumulativeEnergy(), 0.0001);
        double last4CumulativeEnergy = 4 * i * 1.2 * i * 1.2;
        assertEquals(last4CumulativeEnergy, energyCaptor.getLast4CumulativeEnergy(), 0.0001);
        assertEquals(1, energyCaptor.getCumulativeSignalNoiseRatio(), 0.0001);

        energyCaptor.accumulate(new AccumulateEvent(FREQ, 4000.0, countDownLatch));
        Thread.sleep(30);

        last4CumulativeEnergy = 3 * i * 1.2 * i * 1.2 + 4000. * 1.2 * 4000. * 1.2;
        assertEquals(last4CumulativeEnergy, energyCaptor.getLast4CumulativeEnergy(), 0.0001);
        //we should not be able to see in the last 10 events the very last event ^^^ yet.
        assertEquals(last10CumulativeEnergy, energyCaptor.getLast10CumulativeEnergy(), 0.0001);
        assertEquals(1.75, energyCaptor.getCumulativeSignalNoiseRatio(), 0.0001);

    }

    @Test
    public void testCumulativeSignalNoiseRatio() throws InterruptedException {
        IntStream.range(0, 11).forEach(k -> energyCaptor.accumulate(new AccumulateEvent(FREQ, 2999.0, countDownLatch)));

        Thread.sleep(50);
        assertEquals(1, energyCaptor.getCumulativeSignalNoiseRatio(), 0.0001);

        IntStream.range(0, 2).forEach(k -> energyCaptor.accumulate(new AccumulateEvent(FREQ, 30000., countDownLatch)));
        Thread.sleep(50);
        assertEquals(50.53335, energyCaptor.getCumulativeSignalNoiseRatio(), 0.0001);
        IntStream.range(0, 2).forEach(k -> energyCaptor.accumulate(new AccumulateEvent(FREQ, 2999.0, countDownLatch)));
        Thread.sleep(50);
        assertEquals(50.53335, energyCaptor.getCumulativeSignalNoiseRatio(), 0.0001);
    }


    @Test
    public void getMaxEnergyM() throws InterruptedException {
        IntStream.range(20, 80).forEach(i-> energyCaptor.accumulate(new AccumulateEvent(FREQ, (double)i, countDownLatch)));

        energyCaptor.accumulate(new AccumulateEvent(FREQ, 300.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 100.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 80.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 60.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 40.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 20.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 10.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 5.0, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 1.0, countDownLatch));

        Thread.sleep(300);
        logger.debug("getMaxEnergyM running check");
        assertEquals(IntStream.of(20,10,5,1).mapToDouble(i -> i * 1.2).map(i -> i * i).sum(), energyCaptor.getLast4CumulativeEnergy(), 0.0001);
    }

    @Test
    public void getCumEnergy() throws InterruptedException {
        IntStream.range(20, 80).forEach(i-> energyCaptor.accumulate(new AccumulateEvent(FREQ, (double)i, countDownLatch)));

        energyCaptor.accumulate(new AccumulateEvent(FREQ, 6, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 13, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 5, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 140, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 39, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 1_656, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 22_605, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 17_912, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 11_413, countDownLatch));
        energyCaptor.accumulate(new AccumulateEvent(FREQ, 3_887, countDownLatch));

        Thread.sleep(300);
        assertEquals(IntStream.of(6, 13, 5, 140, 39, 1_656)
            .mapToDouble(i -> i * 1.2)
            .map(i -> i * i)
            .sum(), energyCaptor.getLast10CumulativeEnergy(), 0.0001);
    }



    private void check(int i) throws InterruptedException {
        Thread.sleep(50);
        Mockito.verify(callback, times(i)).accept(any(EnergyTriggerEvent.class));
    }



}