package org.amd.dtmfDecoder.analyser;

import edu.princeton.cs.Complex;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 * @author iyuvchenko 2018
 */
public class EventDispatcherTest {
    private IEventDispatcher eventDispatcher;
    private boolean recognized;

    @Mock
    private IRecognizer callback ;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setUp() {
        eventDispatcher = IEventDispatcher.createDefault(callback, DtmfUtils.BLOCK_SIZE / 2);
        recognized = false;
    }

    @Test
    public void dispatch() throws InterruptedException {
        eventDispatcher.dispatch(create(100));
        Thread.sleep(100);
        Mockito.verify(callback).recognize();
    }

    private Complex[] create(double re) {
        final Complex[] yy = new Complex[DtmfUtils.BLOCK_SIZE / 2];

        for (int i = 0; i < yy.length; i++) {
            yy[i] = new Complex(0, 0);
        }

        yy[0] = new Complex(re, 0);
        return yy;
    }
}