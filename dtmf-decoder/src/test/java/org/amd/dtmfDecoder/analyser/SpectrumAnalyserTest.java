package org.amd.dtmfDecoder.analyser;

import org.amd.dtmfDecoder.analyser.dtmf.DtmfEvent;
import org.amd.dtmfDecoder.utils.AudioFileReader;
import org.amd.dtmfDecoder.utils.AudioFileWriter;
import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.amd.dtmfDecoder.utils.SignalGenerator;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static org.junit.Assert.assertEquals;

/**
 * @author iyuvchenko 2018
 */
public class SpectrumAnalyserTest {
    private static final Logger logger = LoggerFactory.getLogger(SpectrumAnalyserTest.class);
    private SpectrumAnalyser spectrumAnalyser;
    private List<DtmfEvent> dtmfEvents;
    private DtmfEvent latestDtmfEvent;

    private final Consumer<DtmfEvent> callback = dtmfEvent -> {
        logger.debug("----------- got DTMF {}", dtmfEvent);
        latestDtmfEvent = dtmfEvent;
        dtmfEvents.add(dtmfEvent);
    };

    @Before
    public void setUp() throws Exception {
        latestDtmfEvent = null;
//        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(EnergyCaptor.class)).setLevel(Level.WARN);
//        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(SpectrumAnalyser.class)).setLevel(Level.WARN);
//        ((ch.qos.logback.classic.Logger) LoggerFactory.getLogger(EventDispatcher.class)).setLevel(Level.WARN);

        final IRecognizer recognizer = new FreqRecognizer(callback);
        final IEventDispatcher eventDispatcher = IEventDispatcher.createDefault(recognizer, DtmfUtils.BLOCK_SIZE / 2);

        spectrumAnalyser = new SpectrumAnalyser(eventDispatcher);
        dtmfEvents = new LinkedList<>();
    }

    @Test
    public void testBLOCK_SIZE() throws InterruptedException {
        short[] silent = SignalGenerator.generateDuration(700, 1000, 0);
        final short[] oneThousand = SignalGenerator.generateDuration(150, 1209, 0.5);
        final short[] silent2 = SignalGenerator.generateDuration(700, 1000, 0);

        silent=DtmfUtils.joinTwoArrays(silent, oneThousand);
        silent=DtmfUtils.joinTwoArrays(silent, silent2);

        new AudioFileWriter(silent, new File("src/test/resources/update.au")).write();

        spectrumAnalyser.submitForAnalysis((silent));
        Thread.sleep(300);
        assertEquals("1", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testMute() throws InterruptedException {
        short[] silent = SignalGenerator.generateDuration(700, 1000, 0);
        short[] oneThousand = SignalGenerator.generateDuration(100, 1209, 0.08);
        short[] silent2 = SignalGenerator.generateDuration(100, 1000, 0);

        silent = DtmfUtils.joinTwoArrays(silent, oneThousand);
        silent = DtmfUtils.joinTwoArrays(silent, silent2);

        new AudioFileWriter(silent, new File("src/test/resources/update.au")).write();

        spectrumAnalyser.submitForAnalysis(silent);
        Thread.sleep(300);
        assertEquals(1, dtmfEvents.size());
        final Predicate<DtmfEvent> filter = event -> event.getValue().equals("1");
        assertEquals(1, dtmfEvents.stream().filter(filter).count());
        spectrumAnalyser.stop();
    }

    @Test
    public void testMute2Events() throws InterruptedException {
        short[] silent = SignalGenerator.generateDuration(600, 1000, 0);

        silent = DtmfUtils.joinTwoArrays(silent, SignalGenerator.generateDuration(100, 1209, 0.08));//signal
        silent = DtmfUtils.joinTwoArrays(silent, SignalGenerator.generateDuration(200, 1000, 0));
        silent = DtmfUtils.joinTwoArrays(silent, SignalGenerator.generateDuration(100, 1209, 0.08));//signal
        silent = DtmfUtils.joinTwoArrays(silent, SignalGenerator.generateDuration(100, 1000, 0));

        new AudioFileWriter(silent, new File("src/test/resources/testMute2Events.au")).write();

        spectrumAnalyser.submitForAnalysis(silent);

        Thread.sleep(600);
        assertEquals(2, dtmfEvents.size());
        final Predicate<DtmfEvent> filter = event-> event.getValue().equals("1") || event.getValue().equals("4");
        assertEquals(2, dtmfEvents.stream().filter(filter).count());
        spectrumAnalyser.stop();
    }

    private void submit(final String fname, int from, int to) throws InterruptedException {
        final short[] shorts = new AudioFileReader(fname).getBodyFragment(from, to);

        spectrumAnalyser.submitForAnalysis(shorts);
        new AudioFileWriter(shorts,new File("src/test/resources/update.au")).write();

        Thread.sleep(300);//it should take 300ms
    }

    private void submit(final String fname) throws InterruptedException {
        final short[] shorts = (new AudioFileReader(fname).getBody());
        spectrumAnalyser.submitForAnalysis(shorts);
        Thread.sleep(1000);//it should take 300ms
    }

    @Test
    public void testSilent() throws InterruptedException {
        int from = 0;
        int to = 7000;
        int durationMs = to - from;

        submit("src/test/resources/silent.au", from, to);
        Thread.sleep(1000);
        assertEquals(0, dtmfEvents.size());
        spectrumAnalyser.stop();
    }


    @Test
    public void testFreqBurst() throws InterruptedException {
        int from = 0;
        int to = 1900;
        int durationMs = to - from;

        submit("src/test/resources/freqBurst.au", from, to);
        Thread.sleep(1000);
        assertEquals(0, dtmfEvents.size());
        spectrumAnalyser.stop();
    }


    @Test
    public void testDtmfStar() throws InterruptedException {
        int from = 701;
        int to = 1700;
        int durationMs = to - from;

        submit("src/test/resources/dtmfStar.au", from, to);

        assertEquals("*", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testDtmf4() throws InterruptedException {
        int from = 0;
        int to = 3000;
        int durationMs = to - from;
        int blocks = (DtmfUtils.SAMPLE_RATE / 1000) * durationMs / DtmfUtils.BLOCK_SIZE;

        submit("src/test/resources/dtmf4.au", from, to);

        assertEquals("4", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testDtmf7() throws InterruptedException {

        int from = 500;
        int to = 1700;
        int durationMs = to - from;
        int blocks = (DtmfUtils.SAMPLE_RATE / 1000) * durationMs / DtmfUtils.BLOCK_SIZE;

        submit("src/test/resources/dtmf7.au", from, to);

        assertEquals("7", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testDtmf1() throws InterruptedException {

//        int from = 321;
        int from = 0;
        int to = 2000;
        int durationMs = to - from;
        int blocks = (DtmfUtils.SAMPLE_RATE/1000)*durationMs/ DtmfUtils.BLOCK_SIZE;

        submit("src/test/resources/dtmf1.au", from, to);

        assertEquals("1", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testDtmf2() throws InterruptedException {

        int from = 0;
        int to = 900;
        int durationMs = to - from;
        int blocks = (DtmfUtils.SAMPLE_RATE / 1000) * durationMs / DtmfUtils.BLOCK_SIZE;

        submit("src/test/resources/dtmf2.au", from, to);
        assertEquals("2", latestDtmfEvent.getValue());
        spectrumAnalyser.stop();
    }

    @Test
    public void testDtmf3() throws InterruptedException {

        int from = 0;
        int to = 2000;
        int durationMs = to - from;
        int blocks = (DtmfUtils.SAMPLE_RATE / 1000) * durationMs / DtmfUtils.BLOCK_SIZE;

        submit("src/test/resources/dtmf3.au", from, to);

        assertEquals("3", latestDtmfEvent.getValue());
        assertEquals(1, dtmfEvents.size());

        spectrumAnalyser.stop();
    }

    private Predicate<DtmfEvent> getFilter(final String expected){
        return dtmfEvent -> dtmfEvent.getValue().equals(expected);
    }

    @Test
    public void testRealtimePound() throws InterruptedException {
        submit("src/test/resources/realtime#.au");
        String expected = "#";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtimeStar() throws InterruptedException {
        submit("src/test/resources/realtime*.au");
        String expected = "*";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime0() throws InterruptedException {
        submit("src/test/resources/realtime0.au");
        String expected = "0";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime1() throws InterruptedException {
        submit("src/test/resources/realtime1.au");
        String expected = "1";
        Thread.sleep(1000);
        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime2() throws InterruptedException {
        submit("src/test/resources/realtime2.au");
        String expected = "2";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime3() throws InterruptedException {
        submit("src/test/resources/realtime3.au");
        String expected = "3";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime4() throws InterruptedException {
        submit("src/test/resources/realtime4.au");
        String expected = "4";
Thread.sleep(1000);
        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime5() throws InterruptedException {
        submit("src/test/resources/realtime5.au");
        String expected = "5";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime6() throws InterruptedException {
        submit("src/test/resources/realtime6.au");
        String expected = "6";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime7() throws InterruptedException {
        submit("src/test/resources/realtime7.au");
        String expected = "7";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime8() throws InterruptedException {
        submit("src/test/resources/realtime8.au");
        String expected = "8";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }

@Test
    public void testRealtime9() throws InterruptedException {
        submit("src/test/resources/realtime9.au");
        String expected = "9";

        assertEquals(dtmfEvents.size(), dtmfEvents.stream().filter(getFilter(expected)).count());
        spectrumAnalyser.stop();
    }



}