package org.amd.dtmfDecoder.analyser.dtmf;

import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * @author iyuvchenko 2018
 */
public class DtmfMapTest {
    @Test
    public void getAllTones() {
        final long i = DtmfMap.ALL_TONES_SET.size();
        assertEquals(8, i);
    }

    @Test
    public void testPredicate() {
        final int i744=744;
        final int i746=746;
        List<Integer> i = Stream.of(i744, i746).filter(DtmfMap.getAuxiliaryTonesPredicate()).collect(Collectors.toList());
        assertFalse(i.contains(i746));
        assertTrue(i.contains(i744));
    }

    @Test
    public void testDtmf() {
        assertTrue(DtmfMap.freqToDtmfEvent(new int[]{1240, 744}).isPresent());
        assertFalse(DtmfMap.freqToDtmfEvent(new int[]{1240, 745}).isPresent());
    }
}