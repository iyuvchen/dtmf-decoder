package org.amd.dtmfDecoder.micCapture;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;

/**
 * @author iyuvchenko 2018
 */
public class MicReaderTest {
    private static final Logger logger = LoggerFactory.getLogger(MicReaderTest.class);
    boolean called;

    @Before
    public void setUp() throws Exception {
        boolean called = false;
    }

    @Test
    public void start() throws InterruptedException {

        final Consumer<short[]> callback = arr-> {
            called=true;
            logger.debug("got {} samples from MixReader", arr.length);
        };
        MicReader micReader = new MicReader(callback);
        Thread t1 = new Thread(micReader);
        t1.start();
        Thread.sleep(500);
        micReader.shutdown();
        Thread.sleep(500);

        assertEquals(called, true);
    }
}