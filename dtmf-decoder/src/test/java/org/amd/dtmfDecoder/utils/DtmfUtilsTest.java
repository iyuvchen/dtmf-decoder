package org.amd.dtmfDecoder.utils;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author iyuvchenko 2018
 */
public class DtmfUtilsTest {

    @Test
    public void joinTwoArrays() {
        short [] arr1 = {1,2,3,4,5,6};
        short [] arr2 = {7,8,9,10,11,12};
        short[] arr3 = DtmfUtils.joinTwoArrays(arr1, arr2);
        assertTrue(Arrays.equals(arr3, new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}));
    }

    @Test
    public void byteArrToShort() {
        short x =100;

        short[] y = DtmfUtils.byteArrToShort(DtmfUtils.shortToByteArr(x));
        assertEquals(y[0], x);
    }

    @Test
    public void readLogo() {
        final String ss = DtmfUtils.readLogo();
        assertFalse(ss.isEmpty());
    }
}