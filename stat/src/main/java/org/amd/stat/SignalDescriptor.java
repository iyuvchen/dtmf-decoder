package org.amd.stat;

import java.util.Objects;

/**
 * @author iyuvchenko 2018
 */
public class SignalDescriptor {
    private final int period;
    private final float ratio;
    private final int silenceDurationMs;

    public SignalDescriptor(final int period, final float ratio, final int silenceDurationMs) {
        this.period = period;
        this.ratio = ratio;
        this.silenceDurationMs = silenceDurationMs;
    }

    public int getSilenceDurationMs() {
        return silenceDurationMs;
    }

    public int getPeriod() {
        return period;
    }

    public float getRatio() {
        return ratio;
    }

    @Override
    public String toString() {
        return "SignalDescriptor{" +
            "period=" + period +
            ", ratio=" + ratio +
            ", silenceDurationMs=" + silenceDurationMs +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SignalDescriptor that = (SignalDescriptor) o;
        return period == that.period &&
            Float.compare(that.ratio, ratio) == 0 &&
            silenceDurationMs == that.silenceDurationMs;
    }

    @Override
    public int hashCode() {
        return Objects.hash(period, ratio, silenceDurationMs);
    }
}
