package org.amd.stat;

import org.amd.dtmfDecoder.utils.DtmfUtils;

import java.util.Arrays;
import java.util.function.Consumer;

import static org.amd.dtmfDecoder.utils.DtmfUtils.SAMPLE_RATE;

/**
 * @author iyuvchenko 2018
 */
public class SignalDescriptorFactory {
    private final static int SIZE = SAMPLE_RATE / 2;
    private final static int BLOCK_DURATION = SIZE * 1000 / SAMPLE_RATE; //500ms
    private final Consumer<SignalDescriptor> callback;
    private int count = 0;
    private int signalDescriptorCount = 0;
    private short[] lastSecondData = {};
    private short[] leftover = {};

    public SignalDescriptorFactory(final Consumer<SignalDescriptor> callback) {
        this.callback = callback;
    }

    static int getSilenceDuration(final int lastZeroPosition) {
        return BLOCK_DURATION - lastZeroPosition / 8;

    }

    static int getSilenceBeginPosition(final short[] data) {
        int silenceBeginPosition = -1;
        for (int i = 0; i < data.length; i++) {
            if (data[i] != 0) {
                silenceBeginPosition = i;
            }
        }
        return silenceBeginPosition;
    }

    private void receiveChunk(final short[] data) {
        if (data.length != SIZE) {
            throw new RuntimeException("data.length != SAMPLE_RATE");
        }
        lastSecondData = data;
        int nonZeroCount = 0;
        for (short s : lastSecondData) {
            if (s != 0) {
                nonZeroCount++;
            }
        }

        callback.accept(new SignalDescriptor(signalDescriptorCount++,
            (float) nonZeroCount / data.length,
            getSilenceDuration(getSilenceBeginPosition(data))));
    }

    public void receive(final short[] data) {
        if (leftover.length == 0) {
            leftover = data;
        } else {
            leftover = DtmfUtils.joinTwoArrays(leftover, data);
        }

        int c = 0;
        while (true) {
            final int from = c;

            int to = c + SIZE;
            if (to > leftover.length) {
                if (from != 0) {
                    to = leftover.length - c + from;
                    leftover = Arrays.copyOfRange(leftover, from, to);
                }
                break;
            }
            receiveChunk(Arrays.copyOfRange(data, from, to));
            c += SIZE;
        }


    }
}
