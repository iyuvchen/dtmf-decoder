package org.amd.stat;

/**
 * @author iyuvchenko 2018
 */
public class Trimmer {
    boolean streemStarted = false;

    public short[] trim(final short[] data) {
        if (streemStarted) {
            return data;
        } else {
            short[] body = null;
            for (int i = 0; i < data.length; i++) {
                if (data[i] == VoiceFilter.MAX && !streemStarted) {
                    streemStarted = true;
                    body = new short[data.length - i];
                    System.arraycopy(data, i, body, 0, data.length - i);
                    return body;
                }
            }
            if (body == null) {
                return new short[]{};
            }
            return body;
        }
    }
}
