package org.amd.stat;

import java.util.Arrays;

/**
 * @author iyuvchenko 2018
 */
public class VoiceFilter {
    public static short MAX = 2245;
    public static short MIN = (short) (2245 * 0.45);

    public static short[] filter(final short[] data) {
        final short[] body = Arrays.copyOf(data, data.length);
        for (int i = 0; i < body.length; i++) {
            if (body[i] < MIN) {
                body[i] = 0;
            } else {
                body[i] = MAX;
            }
        }
        return body;
    }

}
