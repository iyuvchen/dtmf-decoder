from matplotlib import pyplot as plt

text_file = open("test.dat", "r")
lines = text_file.readlines()
lines = [int(i) for i in lines]
marker_style = dict(linestyle=':', color='cornflowerblue', markersize=10)

plt.subplot(2, 1, 1)
plt.plot(lines, **marker_style)
plt.title('A tale of 2 subplots')
plt.ylabel('Damped oscillation')

plt.subplot(2, 1, 2)
plt.plot(lines, **marker_style)
plt.ylabel('plot 2')
plt.show()
