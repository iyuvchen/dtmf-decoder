package org.amd.stat;

import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.type.PhoneNumber;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

/**
 * @author iyuvchenko 2018
 */
public class MakeCall {
    public static void main(String[] a) throws IOException, URISyntaxException, InterruptedException {

        final InputStream is = MakeCall.class.getResourceAsStream("/cred.properties");

        Properties prop = new Properties();
        prop.load(is);

        Twilio.init(prop.getProperty("AccSid"), prop.getProperty("token"));

        String toGoogleVoice = "+14155345305";
        String smcu = "+16503631725";
        String from = "+16502296224";

        Call call = Call.creator(
            new PhoneNumber(smcu),
            new PhoneNumber(from),
            new URI("https://handler.twilio.com/twiml/EHa99b0a71b64ae50fd60d424739509ec3")).create(); //record

        System.out.println(call.getSid());

        Thread.sleep(20_000);
        listAndKillCalls();

        System.out.println();
    }

    public static void listAndKillCalls() throws URISyntaxException {
        ResourceSet<Call> calls = Call.reader().setStatus(Call.Status.IN_PROGRESS)
            .read();

        // Loop over calls and print out a property for each one.
        for (Call call : calls) {
            killCall(call.getSid());
        }

        calls = Call.reader().setStatus(Call.Status.RINGING)
            .read();

        // Loop over calls and print out a property for each one.
        for (Call call : calls) {
            killCall(call.getSid());
        }
    }

    private static void killCall(String callSid) {
        Call call = Call.updater(callSid).setStatus(Call.UpdateStatus.COMPLETED)
            .update();
        System.out.println("Killed call " + callSid);
    }
}
