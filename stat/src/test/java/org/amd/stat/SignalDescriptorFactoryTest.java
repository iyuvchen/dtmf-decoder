package org.amd.stat;

import org.amd.dtmfDecoder.utils.DtmfUtils;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

/**
 * @author iyuvchenko 2018
 */
public class SignalDescriptorFactoryTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    Consumer<SignalDescriptor> callback;

    @Test
    public void silenceBeginPositionTest() {
        short[] data4000 = new short[4000];
        data4000[3000] = 1; //last signal
        int i = SignalDescriptorFactory.getSilenceBeginPosition(data4000);
        assertEquals(3000, i);
        int b = SignalDescriptorFactory.getSilenceDuration(i);
        assertEquals(125, b); //125 ms
    }

    @Test
    public void receive4000() {
        short[] data4000 = new short[4000];
        new SignalDescriptorFactory(callback).receive(data4000);
        Mockito.verify(callback, Mockito.only()).accept(any());
    }

    @Test
    public void receive4001() {
        final SignalDescriptorFactory factory = new SignalDescriptorFactory(callback);
        final short[] data4001 = new short[4001];
        factory.receive(data4001);
        Mockito.verify(callback, Mockito.only()).accept(any());
        factory.receive(new short[1000]);
        Mockito.verify(callback, Mockito.only()).accept(any());
        factory.receive(new short[4000 - 1001]);
        Mockito.verify(callback, Mockito.times(2)).accept(any());
    }

    @Test
    public void receiveFifty() {
        final List<SignalDescriptor> list = new LinkedList<>();
        final Consumer<SignalDescriptor> callback = list::add;

        final SignalDescriptorFactory factory = new SignalDescriptorFactory(callback);
        final short[] data1000 = new short[1000];
        final short[] data1000_3000 = new short[2000];
        Arrays.fill(data1000_3000, (short) 500);

        final short[] arr2 = DtmfUtils.joinTwoArrays(data1000, data1000_3000);
        final short[] arr3 = DtmfUtils.joinTwoArrays(arr2, data1000);
        final int i = SignalDescriptorFactory.getSilenceBeginPosition(arr3);
        final int b = SignalDescriptorFactory.getSilenceDuration(i);
        factory.receive(arr3);

        SignalDescriptor signalDescriptor = list.get(0);
        SignalDescriptor signalDescriptor2 = new SignalDescriptor(0, 0.5f, 126);
        assertEquals(signalDescriptor, signalDescriptor2);
        factory.receive(arr3);
        signalDescriptor = list.get(1);
        signalDescriptor2 = new SignalDescriptor(1, 0.5f, 126);
        assertEquals(signalDescriptor, signalDescriptor2);
    }

}