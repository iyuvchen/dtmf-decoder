package org.amd.stat;

import org.amd.dtmfDecoder.utils.AudioFileReader;
import org.amd.dtmfDecoder.utils.AudioFileWriter;
import org.amd.dtmfDecoder.utils.SignalGenerator;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * @author iyuvchenko 2018
 */
public class SmootherTest {
    private Smoother smoother;

    @Before
    public void setUp() throws Exception {
        smoother = new Smoother();
    }

    @Test
    public void test0() {
        short[] body = new short[60];
        short[] smoothed = smoother.smooth(body);
        assertEquals(smoothed.length, 0);
    }


    @Test
    public void test00() {
        short[] body = new short[128];
        short[] smoothed = smoother.smooth(body);
        assertEquals(smoothed.length, 128);
    }

    @Test
    public void test000() {
        short[] body = SignalGenerator.generateNumberOfSamples(128, 1000, 0.1);

        short[] smoothed = smoother.smooth(body);
//        new AudioFileWriter(body, new File("src/test/resources/plain.au")).write();
//        new AudioFileWriter(smoothed, new File("src/test/resources/iy-hi-smoothed.au")).write();
        assertEquals(smoothed.length, 128);
        assertEquals(smoother.getLeftOver().length, 0);
    }


    @Test
    public void testInvert() {
        short[] body = new AudioFileReader("src/test/resources/iy-do-you-recog-rus.au").getBody();

        short[] inverted = Smoother.invert(body);
        new AudioFileWriter(inverted, new File("src/test/resources/iy-hi-inverted.au")).write();
    }

    @Test
    public void testSmooth() {
        short[] body = new AudioFileReader("src/test/resources/iy-do-you-recog-rus.au").getBody();
        short[] smoothed = smoother.smooth(body);
        new AudioFileWriter(smoothed, new File("src/test/resources/iy-hi-smoothed.au")).write();
    }

    @Test
    public void testSilenceLevel() {
        short[] body = new AudioFileReader("src/test/resources/iy-do-you-recog-rus.au").getBodyFragment(783, 849);

        short[] inverted = Smoother.invert(body);
        new AudioFileWriter(inverted, new File("src/test/resources/iy-hi-inverted.au")).write();

        short[] smoothed = smoother.smooth(body);
        new AudioFileWriter(smoothed, new File("src/test/resources/iy-hi-smoothed.au")).write();
        short max = smoothed[0];
        System.out.println(max);//max=2245
    }

    @Test
    public void testVoiceDetector() {
        short[] body = new AudioFileReader("src/test/resources/iy-do-you-recog-rus.au").getBody();
        short[] smoothed = smoother.smooth(body);
        short[] noNoise = VoiceFilter.filter(smoothed);
        short[] trimmed = new Trimmer().trim(noNoise);

        new AudioFileWriter(smoothed, new File("src/test/resources/iy-hi-smoothed.au")).write();
        new AudioFileWriter(noNoise, new File("src/test/resources/iy-hi-only-voice.au")).write();
        new AudioFileWriter(trimmed, new File("src/test/resources/iy-hi-only-voice-trimmed.au")).write();
    }

    @Test
    public void testSignalDescriptorFactory() {
        short[] body = new AudioFileReader("src/test/resources/iy-do-you-recog-rus.au").getBody();
        short[] smoothed = smoother.smooth(body);
        short[] noNoise = VoiceFilter.filter(smoothed);
        short[] trimmed = new Trimmer().trim(noNoise);

        new SignalDescriptorFactory(null).receive(trimmed);
    }
}